今天從早上開始我就和姐姐大人一起在宅邸中探險喔。現在我們在父親大人的房間裡。

◤父親大人的書房。在巨大的書架上，有很多又難懂又厚的書正在排列著。・・・勉強能讀懂上面的字。閱讀一下書背上的書名吧。啊，看來寫的是關於魔術的樣子。◢

「魔術的書？」
「是～喲。父親大人以前是騎士嘛。所以也需要使用很多魔術喔。很厲害吧。我也想變得能使用很多魔術啊。因為是父親大人的孩子，肯定有使用魔術的適性的，但是魔力量還很少呢？」
「魔術很厲害呢、痛痛都治好了喲！」
「是啊。治癒的魔術比起普通的魔術更需要適性，所以治療師才非常少喲。」
「艾麗娜我，也能使用魔豬魔術嗎？」
「嗚～嗯，會怎樣呢？到正式的適性調查是從６歲開始，所以還不知道啊。有的話就好了呢！」

◤我覺得是有適性呢，但現在的「私」能使用魔術嗎？下次試試看吧。能讀一下這裡的書嗎？◢

「能看書嗎？」
「不行。因為是父親大人重要的書啊。艾麗娜醬是對魔術有興趣嗎？那麼，接下來到我的房間來吧？把我的教科書給妳看哇。」
「嗚嗯，去看書！」
就這樣，來到了姐姐大人的房間了。

◤姐姐大人的房間是和我房間同樣的隔間。只是，書架上有很多很多的書，布偶之類的或是沒見過的玩具？工具？有很多擺放著。窗簾、床等也是被統一用著可愛的圖案和花紋，是像女孩子房間的感覺。◢

姐姐大人從桌子上擺著的那些書中抽出了一本後回到我的身邊，然後把我抱起放在膝蓋上後，就這樣坐在床上。

「這是初等魔術的教科書喔。讓我讀給妳聽吧。」

◤從薄薄的教科書上畫著各種魔法陣的圖畫。書的表面上面用著這個世界的文字寫著初等魔術教本。我一邊聽姐姐大人讀著，我也一邊復讀了那本教科書。看來魔術好像是使用體內的魔素中所生成的魔力來發動的，意圖把咒文作為扳機所具現化的現象。魔素是什麼？魔力為什麼能由魔素生成等，好像在這本教科書裡沒有記載。◢

「艾麗娜我，也想使用魔術呢！」
「是啊～。基礎魔術能讓家庭教師的老師來教我們哇。專業魔術就需要進入學校，如果有魔力適性，魔術課程也可以選擇哇。」

◤姐姐大人的教科書非常薄，上面也只記載著基礎而已。像是什麼樣的基礎魔術能做怎樣的東西，能怎樣之類的，被用於什麼樣的工作等等，只記載了這樣的總論。詳細的魔術用法等好像沒記載。真遺憾。下次要不要偷偷地讀讀父親大人書房裡的書呢？◢

一直把我放在膝蓋上，果然開始累了吧？姐姐大人讀了一半教科書後，便打算繼續探險。下次去隔壁，兄長大人的房間吧。

吭、吭

我敲了一下門後，「好的，請進」兄長大人回應了。

「兄長桑嘛，艾麗娜噠喲。」

◤「私」讓姐姐大人打開門後便進入了房間。兄長大人的房間非常簡潔。床、桌子、書架、衣櫃。床的旁邊有個高大的架子，那裡擺放著從未見過的工具。架子上也擺列著小小的布偶。◢

「呀，艾麗娜來了啊。怎麼了？」
「現在，正和艾麗娜醬在宅邸裡探險喲。」
「我不是在問姐上啊。這樣啊，探險嗎？艾麗娜醬，我也一起去可以嗎？」
兄長大人招著手叫我過來，所以我便過去抱著正坐在椅子上的兄長大人的小腿。

「嗯，可以喲。一起去探險吧。」
兄長大人在撫摸著我的頭。因為很舒服所以很喜歡這樣。

我在兄長大人的房間中左顧右盼，然後靠近了書架的那邊。兄長大人也擁有著很多書啊。我也喜歡讀書，太令人羨慕了。

◤看書背的話，比姐姐大人拿給我看的書還厚，而且看起來很難。兄長大人是學習家嗎？不知道可不可以借給我看呢？◢

「兄長桑嘛，借書給艾麗娜我看可以嗎？」
「這些對艾麗娜來說，有點難懂不是嗎？繪本的話，書庫裡應該還有艾麗娜沒讀過的喲！」
「咻咕書庫？」
「是啊。那麼，接下來進行探險的地方是書庫對吧？」
「嗯，去咻咕書庫～」

就這樣，這次是三個人一起去書庫。書庫是在閣樓。之前的探險中只有３層沒去，所以有點興奮哦。

◤爬著樓梯到閣樓去了，不過，那裡明明是閣樓卻和想像的相差甚遠，走廊上有幾扇門。確實能看到閣樓走廊的盡頭有著斜著設置的小窗戶，雖然稍微有點暗。但是，完全不感到狹窄。◢

兄長大人把在近處房間的門打開了。

「這個房間是書庫。書庫的門沒有鎖，進去也沒問題，但是其他的房間好像有爺爺大人重要的道具，所以不能進去喲。」
「爺爺大人的房間也在這裡喲。就是最裡面的那個房間。但是，總是鎖著，因為爺爺大人也很少回家，所以基本沒有人來這裡哇。」
「爺爺桑嘛的房間為什麼會在閣樓呢？」
「嗯？為什麼呢？我沒問過呢。可能是喜歡高的地方吧？」
「偶爾想在屋頂上躺下，或是想爬到樹頂之類的吧？肯定是這樣。」

◤就像是煙總是喜歡高處的說明呢。說到閣樓裡的房間，一般都是傭人的房間吧？在繪本中也有這樣的故事吧。爺爺大人好像是個很了不起的人，為什麼房間會在這種隱蔽的地方呢？嗯～是喜歡隱蔽的房間嗎？◢

◤書庫裡擺滿了書架，書架上有很多厚厚的書。哇，好厲害啊，好興奮。◢

「艾麗娜我，去讀書也可以嗎？」
「我記得這裡的書應該沒問題。確實這邊的深處有著繪本在才對。」
兄長大人拉著我的手，帶到了放著繪本的書架。對其他的書雖然也有興趣，但最喜歡繪本了喲！

「這是爺爺大人為孤兒院凖備繪本時，多出來的」
兄長大人給我的繪本是，關於勇者打倒魔王的故事。

被神選中授予黃金手鐲的勇者，打倒了從黑暗大陸帶領著魔族的魔王的故事·・・・・這樣想著時，這繪本上記載的還真是真實歷史的樣子。

◤兄長大人一邊給我讀繪本一邊告訴我。魔王好像每隔數百年就會出生一次。在日光無法照射到的黑暗大地上，從魔族領地不斷地向明亮溫暖的大地進攻。但是，每次都會被神所引導的勇者打倒。這個王都好像也是由很久以前的勇者建國的。聽說隔壁的帝國也是，是更早以前的勇者建國的。勇者真厲害啊。只是，關於神和黃金手鐲稍微有點在意呢，不過。我左腕上的手鐲可能就是那個吧，然而呢？鑒於現在也沒有確認的手段，所以保留吧。好想快點摘掉這個令人鬱悶的手鐲啊。◢

看完繪本之後我又看了別的書。雖然寫著有點難懂的字，由我來讀的話還很困難，但是兄長大人和姐姐大人教了我，所以學習到了。

「這個該怎麼讀？」
「誒～哆，是西式燉牛肉飯噠呢。」
「這是？」
「青椒肉絲。」

◤・・・・・・・・・・・・・。◢

「這個呢？還有這個呢？」
「豆瓣醬。」
「牛肉燴飯。」

◤・・・・・・・・・・・・嗯？◢

「兄長桑嘛，什麼叫作豆瓣醬？」
「・・・嗯～？這本書是旅行記，我覺得是國家或土地的名字吧？」
「是這樣啊。真是不可思議的異國或地名吧？」
「這樣啊。・・・・・・」

◤不是異國而是異界美食漫遊記吧？還是說，這是誰帶過來的料理呢？但是，好像推廣得不夠廣呢。◢

此後也讀了各種各樣的書。很開心。下次一個人再來吧。問了一下姐姐大人後，說是和侍女一起的話就沒問題。因為我還小，要是從台階滾下去的話會很危險，又或者書從書架上掉下來的話可能會受傷，所以一個人是不行的。

從第二天後，我就開始一有空就去書庫看書。難懂的字也會問侍女。因為幾乎所有的書都寫著很難懂的詞彙，所以不太明白，但是總覺得很想讀。讀著看不懂的書，感覺頭腦會變好呢。雖然第二天就忘了。但是沒關係。因為很開心呢。

========================================

莉潔「艾麗娜的深層意識想讀那些很難懂的書，所以向表層意識施加壓力了哇！」
菲利昂「過濾器只能容許做出３歲兒童般的行為吧？為什麼，表層意識會想讀書呢？」
莉潔「所謂孩子啊，就算看不懂，只要有興趣，什麼都會去看的吧。跟那個行動原理是一樣的呢！」
菲利昂「原來這是閒得無聊的孩子玩耍的一個環節啊。」
